import React, { useContext } from "react";
import "../App.css";
import LanguageContext from "../context";

const Navbar = () => {
  const { language, handleChangeLanguage } = useContext(LanguageContext);
  const { text } = language;

  return (
    <div className="navbar">
      <p>{text.home}</p>
      <p>{`Idioma actual: ${language.id}`}</p>
      <button onClick={() => handleChangeLanguage("english")}>
        EN
      </button>
      <button onClick={() => handleChangeLanguage("spanish")}>
        ES
      </button>
      <button onClick={() => handleChangeLanguage("portuguese")}>
        PTBR
      </button>
    </div>
  );
};

export default Navbar;
